var module = {
	arr: [], 
	addNumber: function(){
		var additive = prompt('Write the number to add', additive);
		var num = parseFloat(additive);
		if (isNaN(num) == false){
			this.arr.push(num); 
		};
	},
	// removeFirstNumber: function(){
	// 	var removed = this.arr.shift();
	// 	console.log('First element [', removed, '] was removed');
	// 	console.log(module.arr);
	// },
	// removeLastNumber: function(){
	// 	var removed = this.arr.pop();
	// 	console.log('Last element [', removed, '] was removed');
	// 	console.log(module.arr);
	// },
	// removeDefinedNumber: function(start, quantity){
	// 	var start = prompt('Remove elements from N index? N:', start);
	// 	var quantity = prompt('How many numbers to remove?', quantity);
	// 	var returnedElems = this.arr.splice(start, quantity);
	// 	console.log(returnedElems, ' was removed from array');
	// 	console.log('Current array is', this.arr);
	// },
	removeChoosenNumber: function(){
		var numToRemove = parseInt(prompt('Number to remove', numToRemove));
		var indexOfRemoved = this.arr.indexOf(numToRemove);
		var returnedElems = this.arr.splice(indexOfRemoved, 1);
		console.log(returnedElems, ' was removed from array');
	},
	isInclude: function(){
		var numToCheck = prompt('Does number below belong to array?', numToCheck);
		var num = parseFloat(numToCheck);
		if (this.arr.indexOf(num) != -1){
			console.log(num, 'is included');
		} else {
			console.log(num, 'is absent');
		}
	},
	printSumm: function(){
		console.log(this.arr.reduce((sum, currentValue) => sum + currentValue, 0));
	},
	printAbsSum: function(){
		console.log(Math.abs(this.arr.reduce((sum, currentValue) => sum + currentValue, 0)));
	},
	isAllPositive: function(){
		flag = true;
		counter = 0;
		for (var i = this.arr.length - 1; i >= 0; i--) {
			if (this.arr[i] < 0) {
				flag = false;
				counter ++;
			}	
		}
		if (flag) {
			console.log('There is no negative numbers')
		} else {
			console.log('There is ', counter, ' negative numbers')
		}

	},
	printOnlyEven: function(){
		console.log('Even numbers of array:');
		for (var i = this.arr.length - 1; i >= 0; i--) {
			if (this.arr[i] % 2 == 0) {
				console.log(this.arr[i])
			}	
		}
	},
	multiplyAllOnNumber: function(x){
		var numToMultiply = parseFloat(prompt('Number to multiply on', numToMultiply));
		console.log(this.arr.map(elem => elem * numToMultiply));
	}
};

var quant = prompt('How many numbers to add?', quant);
for (var i = quant - 1; i >= 0; i--) {
	module.addNumber();
};
console.log('Original array: ', module.arr);
// module.removeFirstNumber();
// module.removeLastNumber();
// module.removeDefinedNumber();
module.removeChoosenNumber();
module.isInclude();
module.printSumm();
module.printAbsSum();
module.isAllPositive();
module.printOnlyEven();
module.multiplyAllOnNumber();